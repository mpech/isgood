#!/usr/bin/env bash
set -x -e

cd ../isgood-data/

pijul channel new mike1
for H in $(pijul credit Makefile | grep -o "^[[:alnum:]]*" | sort -u | head -2); do
	pijul apply --channel mike1 "$H"
done
