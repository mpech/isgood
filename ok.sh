#!/usr/bin/env bash
set -x -e

cd ../isgood-data
i=1
for H in $(pijul log --hash-only); do
	pijul channel new "$i"
	pijul apply "$H" --channel "$i"
	
	pijul channel switch "$i"
	# pijul debug
	pijul channel switch main
	
	i=$((i+1))
done
