#!/usr/bin/env bash
set -x -e

./clean.sh

mkdir ../isgood-data
cp -R data/* ../isgood-data/
cp -R repo   ../isgood-data/.pijul
cd ../isgood-data
pijul reset
