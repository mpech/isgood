#!/usr/bin/env bash
set -x -e

cd ..
rm -rf isgood-data
mkdir isgood-data
cd isgood-data
pijul init try
cd try

touch a b c d
pijul add a b c d
pijul record -am.
H=$(pijul log --hash-only | head -1)

for i in {1..1024}; do
	pijul channel new "$i"
	pijul apply "$H" --channel "$i"
done
